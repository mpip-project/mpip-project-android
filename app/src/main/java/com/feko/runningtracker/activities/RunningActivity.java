package com.feko.runningtracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.feko.runningtracker.R;
import com.feko.runningtracker.helpers.ServiceHelper;
import com.feko.runningtracker.services.RunningService;

public class RunningActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running);

        Glide.with(this).asGif().load(R.drawable.running).into((ImageView) findViewById(R.id.ivGif));

        if (!ServiceHelper.isServiceRunning(RunningService.class, this)) {
            Intent runningServiceIntent = new Intent(this, RunningService.class);
            startService(runningServiceIntent);
        }
    }

    public void stopRunning(View view) {
        Intent runningServiceIntent = new Intent(this, RunningService.class);
        stopService(runningServiceIntent);

        Intent homeActivityIntent = new Intent(this, HomeActivity.class);
        startActivity(homeActivityIntent);
    }
}
