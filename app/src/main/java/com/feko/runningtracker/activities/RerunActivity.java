package com.feko.runningtracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.feko.runningtracker.PreferenceVariables;
import com.feko.runningtracker.R;
import com.feko.runningtracker.helpers.PersistenceHelper;
import com.feko.runningtracker.helpers.RerunAdapter;
import com.feko.runningtracker.helpers.RetrofitHelper;
import com.feko.runningtracker.helpers.SharedPreferencesHelper;
import com.feko.runningtracker.persistence.dao.CheckpointDao;
import com.feko.runningtracker.persistence.dao.RerunDao;
import com.feko.runningtracker.persistence.models.Checkpoint;
import com.feko.runningtracker.persistence.models.Rerun;
import com.feko.runningtracker.persistence.models.RerunFull;
import com.feko.runningtracker.retrofit.RerunService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RerunActivity extends AppCompatActivity {

    private ListView lvReruns;
    private TextView tvInfo;

    private String apiKey;
    private Integer remoteRerunCount = 0;
    private RerunDao rerunDao;
    private RerunService rerunService;
    private CheckpointDao checkpointDao;
    private CompositeDisposable compositeDisposable;
    private RerunAdapter rerunAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkApiKey();
        setContentView(R.layout.activity_rerun);
        fetchReferences();
        loadResources();

        lvReruns.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent rerunDetailsIntent = new Intent(RerunActivity.this, RerunDetailsActivity.class);
                rerunDetailsIntent.putExtra(RerunDetailsActivity.RERUN_ID_KEY, l);
                startActivity(rerunDetailsIntent);
            }
        });
    }

    private void loadResources() {
        RerunService rerunService = RetrofitHelper.getRetrofitInstance(ScalarsConverterFactory.create(), RerunService.class, this);
        Call<Integer> rerunServiceCall = rerunService.rerunCountForUserWithKey(apiKey);
        rerunServiceCall.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                onRerunCountReceived(response);
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                remoteRerunCount = -1;
                handleReruns();
            }
        });
    }

    private void showError(int stringId) {
        String error = getResources().getString(stringId);
        tvInfo.setText(error);
    }

    private void onRerunCountReceived(Response<Integer> response) {
        if (response.code() == 200) {
            remoteRerunCount = response.body();
            handleReruns();
        } else {
            showError(R.string.something_wrong);
        }
    }

    private void handleReruns() {
        final List<Rerun> localReruns = new ArrayList<>();
        compositeDisposable.add(
                rerunDao.findAllFull().subscribeOn(Schedulers.io()).subscribe(new Consumer<List<RerunFull>>() {
                    @Override
                    public void accept(List<RerunFull> reruns) {
                        for (RerunFull rerunFull : reruns) {
                            Rerun rerun = new Rerun(rerunFull);
                            localReruns.add(rerun);
                        }
                        int localRerunsSize = trueLocalRerunsSize(localReruns);
                        if (localRerunsSize < remoteRerunCount) {
                            Call<List<Rerun>> remoteRerunsCalls = rerunService.getAll(apiKey);
                            remoteRerunsCalls.enqueue(new Callback<List<Rerun>>() {
                                @Override
                                public void onResponse(Call<List<Rerun>> call, Response<List<Rerun>> response) {
                                    onRemoteRerunResponse(response, localReruns);
                                }

                                @Override
                                public void onFailure(Call<List<Rerun>> call, Throwable t) {
                                    uploadLocallyStoredReruns(localReruns);
                                }
                            });
                        } else {
                            uploadLocallyStoredReruns(localReruns);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        uploadLocallyStoredReruns(localReruns);
                    }
                }));
    }

    private void onRemoteRerunResponse(Response<List<Rerun>> response, final List<Rerun> localReruns) {
        if (response.code() == 200) {
            List<Rerun> remoteReruns = response.body();
            for (final Rerun rerun : remoteReruns) {
                boolean isLocallySaved = false;
                for (Rerun localRerun : localReruns) {
                    if (localRerun.getId() == rerun.getId()) {
                        isLocallySaved = true;
                        break;
                    }
                }
                if (!isLocallySaved) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            long localId = rerunDao.save(rerun);
                            rerun.setLocalId(localId);
                            for (Checkpoint cp : rerun.getCheckpoints()) {
                                cp.setRerunId(localId);
                                checkpointDao.save(cp);
                            }
                            localReruns.add(rerun);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    rerunAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }).start();
                }
            }
            uploadLocallyStoredReruns(localReruns);
        } else {
            showError(R.string.something_wrong);
        }
    }

    private int trueLocalRerunsSize(List<Rerun> localReruns) {
        int localRerunsSize = 0;
        for (Rerun rerun : localReruns) {
            if (rerun.getId() != null) {
                localRerunsSize++;
            }
        }
        return localRerunsSize;
    }

    private void uploadLocallyStoredReruns(List<Rerun> localReruns) {
        for (final Rerun rerun : localReruns) {
            if (rerun.getId() == null) {
                compositeDisposable.add(rerunDao.findFullByLocalId(rerun.getLocalId()).subscribeOn(Schedulers.io()).subscribe(new Consumer<RerunFull>() {
                    @Override
                    public void accept(RerunFull rerunFull) {
                        Call<Rerun> rerunServiceCall = rerunService.postRerun(apiKey, rerunFull);
                        rerunServiceCall.enqueue(new Callback<Rerun>() {
                            @Override
                            public void onResponse(Call<Rerun> call, Response<Rerun> response) {
                                onSuccessfulRerunUpload(response, rerun);
                            }

                            @Override
                            public void onFailure(Call<Rerun> call, Throwable t) {
                            }
                        });
                    }
                }));
            }
        }
        populateListView(localReruns);
    }

    private void populateListView(final List<Rerun> localReruns) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (rerunAdapter == null) {
                    rerunAdapter = new RerunAdapter(localReruns, RerunActivity.this);
                }
                lvReruns.setAdapter(rerunAdapter);
            }
        });
    }

    private void onSuccessfulRerunUpload(final Response<Rerun> response, final Rerun rerun) {
        if (response.code() == 201) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Rerun savedRerun = response.body();
                    rerun.setId(savedRerun.getId());
                    rerunDao.update(rerun);
                }
            }).start();
        } else {
            showError(R.string.something_wrong);
        }
    }

    private void fetchReferences() {
        lvReruns = findViewById(R.id.lvReruns);
        tvInfo = findViewById(R.id.tvInfo);
        rerunDao = PersistenceHelper.getDatabaseInstance(this).rerunDao();
        checkpointDao = PersistenceHelper.getDatabaseInstance(this).checkpointDao();
        rerunService = RetrofitHelper.getRetrofitInstance(GsonConverterFactory.create(), RerunService.class, this);
        compositeDisposable = new CompositeDisposable();
    }

    private void checkApiKey() {
        String apiKey = SharedPreferencesHelper.getString(PreferenceVariables.ApiKey.name(), MODE_PRIVATE, this);
        if (apiKey == null) {
            Intent registerIntent = new Intent(this, RegisterActivity.class);
            startActivity(registerIntent);
        }
        this.apiKey = apiKey;
    }

    @Override
    protected void onDestroy() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.clear();
        }
        super.onDestroy();
    }
}
