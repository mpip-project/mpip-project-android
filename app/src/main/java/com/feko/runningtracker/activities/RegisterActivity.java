package com.feko.runningtracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.feko.runningtracker.PreferenceVariables;
import com.feko.runningtracker.R;
import com.feko.runningtracker.helpers.PersistenceHelper;
import com.feko.runningtracker.helpers.RetrofitHelper;
import com.feko.runningtracker.helpers.ServiceHelper;
import com.feko.runningtracker.helpers.SharedPreferencesHelper;
import com.feko.runningtracker.persistence.RerunDatabase;
import com.feko.runningtracker.retrofit.ApiKeyService;
import com.feko.runningtracker.services.RunningService;

import org.apache.commons.validator.routines.EmailValidator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    private EditText etEmail;
    private TextView tvValidationText;
    private Button bRegister;
    private Button bRelogin;

    private ApiKeyService apiKeyService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkRunning();
        checkApiKey();
        // PreferenceManager.getDefaultSharedPreferences(this).edit().remove("ApiKey").apply();
        final RerunDatabase database = PersistenceHelper.getDatabaseInstance(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                database.rerunDao().nukeTable();
            }
        }).start();

        setContentView(R.layout.activity_register);
        fetchReferences();
        etEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_ENTER) {
                    bRegister.callOnClick();
                    return true;
                }
                return false;
            }
        });
    }

    private void checkRunning() {
        if (ServiceHelper.isServiceRunning(RunningService.class, this)) {
            Intent runningIntent = new Intent(this, RunningActivity.class);
            startActivity(runningIntent);
        }
    }

    public void register(View view) {
        boolean isEmailValid = validateEmail();

        if (isEmailValid) {
            String email = etEmail.getText().toString();
            Call<String> apiKeyCall = apiKeyService.register(email);
            apiKeyCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    OnApiKeyReceived(response);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    OnRegisterError();
                }
            });
        }
    }

    public void recover(View view) {
        String emailKey = getResources().getString(R.string.email_bundle_key);
        String emailValue = etEmail.getText().toString();
        Intent recoveryTokenIntent = new Intent(this, RecoveryTokenActivity.class);
        recoveryTokenIntent.putExtra(emailKey, emailValue);
        startActivity(recoveryTokenIntent);
    }

    private void showValidationError(int stringId) {
        tvValidationText.setText(stringId);
    }

    private void hideValidationText() {
        tvValidationText.setText(R.string.empty);
    }

    public void OnApiKeyReceived(Response<String> response) {
        if (response.code() == 201) {
            SharedPreferencesHelper.putString(PreferenceVariables.ApiKey.name(), response.body(), this);
            Intent homeIntent = new Intent(this, HomeActivity.class);
            startActivity(homeIntent);
        } else if (response.code() == 400) {
            showValidationError(R.string.email_taken_error);
        }
    }

    public void OnRegisterError() {
        showValidationError(R.string.no_network_error);
    }

    private void checkApiKey() {
        String apiKey = SharedPreferencesHelper.getString(PreferenceVariables.ApiKey.name(), MODE_PRIVATE, this);

        if (apiKey != null) {
            Intent homeIntent = new Intent(this, HomeActivity.class);
            startActivity(homeIntent);
        }
    }

    private void fetchReferences() {
        etEmail = findViewById(R.id.etToken);
        tvValidationText = findViewById(R.id.tvValidationText);
        bRegister = findViewById(R.id.bRegister);
        bRelogin = findViewById(R.id.bRecover);

        apiKeyService = RetrofitHelper.getRetrofitInstance(ScalarsConverterFactory.create(), ApiKeyService.class, this);
    }

    private boolean validateEmail() {
        EmailValidator emailValidator = EmailValidator.getInstance();
        String email = etEmail.getText().toString();
        boolean isEmailValid = emailValidator.isValid(email);

        if (!isEmailValid) {
            showValidationError(R.string.invalid_email_error);
        }

        return isEmailValid;
    }

    public void onStartTypingEmail(View view) {
        hideValidationText();
    }
}
