package com.feko.runningtracker.activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.feko.runningtracker.PreferenceVariables;
import com.feko.runningtracker.R;
import com.feko.runningtracker.helpers.PersistenceHelper;
import com.feko.runningtracker.helpers.SharedPreferencesHelper;
import com.feko.runningtracker.persistence.RerunDatabase;
import com.feko.runningtracker.persistence.models.Checkpoint;
import com.feko.runningtracker.persistence.models.RerunFull;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Calendar;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class RerunDetailsActivity extends AppCompatActivity {
    public static final String RERUN_ID_KEY = "rerun key";

    private SupportMapFragment smfMap;
    private TextView tvDistance;
    private TextView tvTime;
    private Button bShare;

    private GoogleMap googleMap;
    private Long rerunId;
    private String time;
    private RerunFull rerunFull;

    private CompositeDisposable compositeDisposable;
    private RerunDatabase rerunDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkApiKey();
        setContentView(R.layout.activity_rerun_details);
        fetchReferences();
        fetchRerunFull();
    }

    private void fetchRerunFull() {
        if (rerunId < 0) {
            Intent rerunIntent = new Intent(this, RerunActivity.class);
            startActivity(rerunIntent);
        }
        compositeDisposable.add(rerunDatabase.rerunDao().findFullById(rerunId).subscribeOn(Schedulers.io()).subscribe(new Consumer<RerunFull>() {
            @Override
            public void accept(RerunFull rerunFull) {
                RerunDetailsActivity.this.rerunFull = rerunFull;
                populateScreen();
            }
        }));
    }

    private void waitForMap() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (googleMap == null) ;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        populateMap();
                    }
                });
            }
        }).start();
    }

    private void populateScreen() {
        tvDistance.setText(String.format("Distance: %sm", rerunFull.getDistance()));
        time = calculateTime();
        tvTime.setText(String.format("Time: %s", time));
        waitForMap();
    }

    private void populateMap() {
        Checkpoint first = rerunFull.getCheckpoints().get(0);
        Checkpoint last = rerunFull.getCheckpoints().get(rerunFull.getCheckpoints().size() - 1);
        LatLng startPoint = new LatLng(first.getLat(), first.getLng());
        LatLng endPoint = new LatLng(last.getLat(), last.getLng());
        LatLng centerPoint = new LatLng((last.getLat() + first.getLat()) / 2, (last.getLng() + first.getLng()) / 2);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(centerPoint, 4));
        googleMap.addMarker(new MarkerOptions().position(startPoint).title("Start"));
        googleMap.addMarker(new MarkerOptions().position(endPoint).title("Finish"));
        PolylineOptions polylineOptions = new PolylineOptions().color(Color.CYAN);
        for (Checkpoint checkpoint : rerunFull.getCheckpoints()) {
            LatLng currentPoint = new LatLng(checkpoint.getLat(), checkpoint.getLng());
            polylineOptions.add(currentPoint);
        }
        googleMap.addPolyline(polylineOptions);
    }

    private String calculateTime() {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(rerunFull.getCheckpoints().get(0).getTimestamp());
        cal2.setTime(rerunFull.getCheckpoints().get(rerunFull.getCheckpoints().size() - 1).getTimestamp());
        long diffInMillis = cal2.getTimeInMillis() - cal1.getTimeInMillis();
        long hours = diffInMillis / (1000 * 60 * 60);
        diffInMillis = diffInMillis % (1000 * 60 * 60);
        long minutes = diffInMillis / (1000 * 60);
        diffInMillis = diffInMillis % (1000 * 60);
        long seconds = diffInMillis / (1000);
        return String.format("%dh %dm %ds", hours, minutes, seconds);
    }

    private void fetchReferences() {
        smfMap = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.smfMap);
        smfMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                RerunDetailsActivity.this.googleMap = googleMap;
            }
        });
        tvDistance = findViewById(R.id.tvDistance);
        tvTime = findViewById(R.id.tvTime);
        bShare = findViewById(R.id.bShare);
        compositeDisposable = new CompositeDisposable();
        rerunId = getIntent().getLongExtra(RERUN_ID_KEY, -1);
        rerunDatabase = PersistenceHelper.getDatabaseInstance(this);
    }

    private void checkApiKey() {
        String apiKey = SharedPreferencesHelper.getString(PreferenceVariables.ApiKey.name(), MODE_PRIVATE, this);
        if (apiKey == null) {
            Intent registerIntent = new Intent(this, RegisterActivity.class);
            startActivity(registerIntent);
        }
    }

    @Override
    protected void onDestroy() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.clear();
        }
        super.onDestroy();
    }

    public void share(View v) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My new run");
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, String.format("I just ran %s meters in %s. Measured with Rerun.", rerunFull.getDistance(), time));
        startActivity(Intent.createChooser(shareIntent, "Share via"));
    }
}
