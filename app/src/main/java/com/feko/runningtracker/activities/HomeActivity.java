package com.feko.runningtracker.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.feko.runningtracker.PreferenceVariables;
import com.feko.runningtracker.R;
import com.feko.runningtracker.helpers.SharedPreferencesHelper;
import com.feko.runningtracker.services.RunningService;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkApiKey();
        setContentView(R.layout.activity_home);
    }

    private void checkApiKey() {
        String apiKey = SharedPreferencesHelper.getString(PreferenceVariables.ApiKey.name(), MODE_PRIVATE, this);
        if (apiKey == null) {
            Intent registerIntent = new Intent(this, RegisterActivity.class);
            startActivity(registerIntent);
        }
    }

    public void onRun(View v) {
        Intent runningIntent = new Intent(this, RunningActivity.class);
        startActivity(runningIntent);
    }

    public void onMyRuns(View v) {
        Intent myRunsIntent = new Intent(this, RerunActivity.class);
        startActivity(myRunsIntent);
    }
}
