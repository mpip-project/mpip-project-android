package com.feko.runningtracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.feko.runningtracker.PreferenceVariables;
import com.feko.runningtracker.R;
import com.feko.runningtracker.helpers.RetrofitHelper;
import com.feko.runningtracker.helpers.SharedPreferencesHelper;
import com.feko.runningtracker.retrofit.ApiKeyService;

import org.apache.commons.validator.routines.EmailValidator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RecoveryTokenActivity extends AppCompatActivity {

    private EditText etEmail;
    private TextView tvInfo;
    private Button bRelogin;

    private ApiKeyService apiKeyService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkApiKey();

        setContentView(R.layout.activity_recovery_token);
        fetchReferences();
        getExtras();
        etEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_ENTER) {
                    bRelogin.callOnClick();
                    return true;
                }
                return false;
            }
        });
    }

    private void checkApiKey() {
        String apiKey = SharedPreferencesHelper.getString(PreferenceVariables.ApiKey.name(), MODE_PRIVATE, this);
        if (apiKey != null) {
            Intent homeIntent = new Intent(this, HomeActivity.class);
            startActivity(homeIntent);
        }
    }

    private void fetchReferences() {
        etEmail = findViewById(R.id.etToken);
        tvInfo = findViewById(R.id.tvError);
        bRelogin = findViewById(R.id.bRelogin);

        apiKeyService = RetrofitHelper.getRetrofitInstance(ScalarsConverterFactory.create(), ApiKeyService.class, this);
    }

    public void getExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String emailKey = getResources().getString(R.string.email_bundle_key);
            String email = extras.getString(emailKey);
            if (email != null) {
                etEmail.setText(email);
            }
        }
    }

    public void relogin(View view) {
        boolean isEmailValid = validateEmail();
        String email = etEmail.getText().toString();

        if (isEmailValid) {
            Call<String> apiKeyCall = apiKeyService.recover(email);
            apiKeyCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    onSentRecoveryToken(response);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    onNoNetwork();
                }
            });
        } else if (email.isEmpty()) {
            Intent reloginIntent = new Intent(this, ReloginActivity.class);
            startActivity(reloginIntent);
        }
    }

    private void onNoNetwork() {
        showError(R.string.no_network_error);
    }

    private void onSentRecoveryToken(Response response) {
        int responseCode = response.code();
        if (responseCode == 200) {
            Intent reloginIntent = new Intent(this, ReloginActivity.class);
            startActivity(reloginIntent);
        } else if (responseCode == 400) {
            showError(R.string.email_isnt_registered);
        }
    }

    private boolean validateEmail() {
        String email = etEmail.getText().toString();
        EmailValidator emailValidator = EmailValidator.getInstance();
        boolean isEmailValid = emailValidator.isValid(email);
        if (!isEmailValid) {
            showError(R.string.invalid_email_error);
        }
        return isEmailValid;
    }

    private void showError(int errorId) {
        String errorMessage = getResources().getString(errorId);
        tvInfo.setText(errorMessage);
    }

    public void hideError(View view) {
        tvInfo.setText(R.string.recovery_token_et_description);
    }
}
