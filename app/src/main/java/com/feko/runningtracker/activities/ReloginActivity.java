package com.feko.runningtracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.feko.runningtracker.PreferenceVariables;
import com.feko.runningtracker.R;
import com.feko.runningtracker.helpers.RetrofitHelper;
import com.feko.runningtracker.helpers.SharedPreferencesHelper;
import com.feko.runningtracker.retrofit.ApiKeyService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ReloginActivity extends AppCompatActivity {
    private EditText etToken;
    private TextView tvError;
    private Button bRelogin;

    private ApiKeyService apiKeyService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkApiKey();

        setContentView(R.layout.activity_relogin);
        fetchReferences();
        etToken.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_ENTER) {
                    bRelogin.callOnClick();
                    return true;
                }
                return false;
            }
        });
    }

    private void fetchReferences() {
        etToken = findViewById(R.id.etToken);
        tvError = findViewById(R.id.tvError);
        bRelogin = findViewById(R.id.bRelogin);

        apiKeyService = RetrofitHelper.getRetrofitInstance(ScalarsConverterFactory.create(), ApiKeyService.class, this);
    }

    private void checkApiKey() {
        String apiKey = SharedPreferencesHelper.getString(PreferenceVariables.ApiKey.name(), MODE_PRIVATE, this);
        if (apiKey != null) {
            Intent homeIntent = new Intent(this, HomeActivity.class);
            startActivity(homeIntent);
        }
    }

    public void relogin(View view) {
        boolean tokenValid = validateToken();

        if (tokenValid) {
            String token = etToken.getText().toString();
            Call<String> apiKeyCall = apiKeyService.relogin(token);
            apiKeyCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    onResponseReceived(response);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    onNetworkFailure();
                }
            });
        }
    }

    private void onNetworkFailure() {
        showError(R.string.no_network_error);
    }

    private void onResponseReceived(Response<String> response) {
        if (response.code() == 200) {
            String apiKey = response.body();
            SharedPreferencesHelper.putString(PreferenceVariables.ApiKey.name(), apiKey, this);
            Intent homeIntent = new Intent(this, HomeActivity.class);
            startActivity(homeIntent);
        } else if (response.code() == 400) {
            showError(R.string.token_expired);
        }
    }

    private boolean validateToken() {
        String token = etToken.getText().toString();
        if (token.length() != 6) {
            showError(R.string.invalid_token_format);
            return false;
        }
        return true;
    }

    private void showError(int stringId) {
        String error = getResources().getString(stringId);
        tvError.setText(error);
    }

    public void hideError(View view) {
        showError(R.string.empty);
    }
}
