package com.feko.runningtracker;

public enum PreferenceVariables {
    ApiKey;

    private static String simpleName = "PreferenceVariables";

    public static String getSimpleName() {
        return simpleName;
    }
}
