package com.feko.runningtracker.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.feko.runningtracker.persistence.dao.CheckpointDao;
import com.feko.runningtracker.persistence.dao.RerunDao;
import com.feko.runningtracker.persistence.models.Checkpoint;
import com.feko.runningtracker.persistence.models.Rerun;

@Database(entities = {Rerun.class, Checkpoint.class}, version = 2, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class RerunDatabase extends RoomDatabase {
    public abstract RerunDao rerunDao();
    public abstract CheckpointDao checkpointDao();
}
