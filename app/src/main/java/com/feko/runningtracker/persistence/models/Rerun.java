package com.feko.runningtracker.persistence.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Relation;

import java.util.List;

@Entity
public class Rerun {
    @PrimaryKey(autoGenerate = true)
    private Long localId;

    private Long id;

    private Double distance;

    @Ignore
    private List<Checkpoint> checkpoints;

    public Rerun() {
    }

    @Ignore
    public Rerun(Long localId, Long id, Double distance, List<Checkpoint> checkpoints) {
        this.localId = localId;
        this.id = id;
        this.distance = distance;
        this.checkpoints = checkpoints;
    }

    @Ignore
    public Rerun(RerunFull rerunFull) {
        this(rerunFull.getLocalId(), rerunFull.getId(), rerunFull.getDistance(), rerunFull.getCheckpoints());
    }

    public Long getLocalId() {
        return localId;
    }

    public Long getId() {
        return id;
    }

    public Double getDistance() {
        return distance;
    }

    public List<Checkpoint> getCheckpoints() {
        return checkpoints;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public void setCheckpoints(List<Checkpoint> checkpoints) {
        this.checkpoints = checkpoints;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }
}
