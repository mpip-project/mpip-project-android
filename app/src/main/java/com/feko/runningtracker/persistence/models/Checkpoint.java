package com.feko.runningtracker.persistence.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.time.LocalDateTime;
import java.util.Date;


@Entity(
        indices = {@Index(value = "localId", name = "id", unique = true),
                @Index(value = "rerunId", name = "rerunId")},
        foreignKeys = @ForeignKey(childColumns = "rerunId",
                entity = Rerun.class,
                parentColumns = "localId",
                onDelete = ForeignKey.CASCADE))
public class Checkpoint {
    @PrimaryKey(autoGenerate = true)
    private Long localId;

    private Long id;

    private Double lat;
    private Double lng;
    private Double alt;

    private Date timestamp;

    private Long rerunId;

    public Checkpoint() {
    }

    @Ignore
    public Checkpoint(Double lat, Double lng, Double alt, Date timestamp, Long rerunId) {
        this.lat = lat;
        this.lng = lng;
        this.alt = alt;
        this.timestamp = timestamp;
        this.rerunId = rerunId;
    }

    public Long getLocalId() {
        return localId;
    }

    public Long getId() {
        return id;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public Double getAlt() {
        return alt;
    }

    public Long getRerunId() {
        return rerunId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void setAlt(Double alt) {
        this.alt = alt;
    }

    public void setRerunId(Long rerunId) {
        this.rerunId = rerunId;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
