package com.feko.runningtracker.persistence.models;

import android.arch.persistence.room.Relation;

import java.util.List;

public class RerunFull {
    private Long localId;
    private Long id;
    private Double distance;
    @Relation(entity = Checkpoint.class, parentColumn = "localId", entityColumn = "rerunId")
    private List<Checkpoint> checkpoints;

    public RerunFull() {
    }

    public Long getLocalId() {
        return localId;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public List<Checkpoint> getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(List<Checkpoint> checkpoints) {
        this.checkpoints = checkpoints;
    }
}
