package com.feko.runningtracker.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

import com.feko.runningtracker.persistence.models.Checkpoint;

@Dao
public interface CheckpointDao {
    @Insert
    long save(Checkpoint checkpoint);
}
