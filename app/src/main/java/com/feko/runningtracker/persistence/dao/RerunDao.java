package com.feko.runningtracker.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.feko.runningtracker.persistence.models.Rerun;
import com.feko.runningtracker.persistence.models.RerunFull;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Path;

@Dao
public interface RerunDao {
    @Query("SELECT * FROM rerun")
    Single<List<Rerun>> findAll();

    @Transaction
    @Query("SELECT localId, id, distance FROM rerun")
    Single<List<RerunFull>> findAllFull();

    @Transaction
    @Query("SELECT localId, distance FROM rerun WHERE localId = :localId")
    Single<RerunFull> findFullByLocalId(@Path(value = "localId") Long localId);

    @Transaction
    @Query("SELECT localId, distance FROM rerun WHERE id = :id")
    Single<RerunFull> findFullById(@Path(value = "id") Long id);

    @Update
    void update(Rerun rerun);

    @Insert
    long save(Rerun rerun);

    @Query("DELETE FROM rerun")
    void nukeTable();
}
