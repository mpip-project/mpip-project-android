package com.feko.runningtracker.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiKeyService {
    @GET("apikey/register/{email}")
    Call<String> register(@Path("email") String email);

    @GET("apikey/recover/{email}")
    Call<String> recover(@Path("email") String email);

    @GET("apikey/relogin/{token}")
    Call<String> relogin(@Path("token") String token);
}
