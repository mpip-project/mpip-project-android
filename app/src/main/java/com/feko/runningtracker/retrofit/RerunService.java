package com.feko.runningtracker.retrofit;

import com.feko.runningtracker.persistence.models.Rerun;
import com.feko.runningtracker.persistence.models.RerunFull;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RerunService {
    @GET(value = "rerun/count/{apiKey}")
    Call<Integer> rerunCountForUserWithKey(@Path("apiKey") String apiKey);

    @POST(value = "rerun/{apiKey}")
    Call<Rerun> postRerun(@Path("apiKey") String apiKey, @Body RerunFull rerun);

    @GET(value = "rerun/{apiKey}")
    Call<List<Rerun>> getAll(@Path("apiKey") String apiKey);
}
