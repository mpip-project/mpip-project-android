package com.feko.runningtracker.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.feko.runningtracker.R;
import com.feko.runningtracker.persistence.models.Rerun;

import java.util.List;
import java.util.Locale;

public class RerunAdapter extends BaseAdapter {

    private List<Rerun> reruns;
    private Context context;

    public RerunAdapter(List<Rerun> reruns, Context context) {
        this.reruns = reruns;
        this.context = context;
    }

    @Override
    public int getCount() {
        return reruns.size();
    }

    @Override
    public Object getItem(int i) {
        return reruns.get(i);
    }

    @Override
    public long getItemId(int i) {
        return reruns.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).
                    inflate(R.layout.rerun_item, viewGroup, false);
        }

        Rerun rerun = reruns.get(i);

        TextView tvRerunNo = view.findViewById(R.id.tvRerunNo);
        TextView tvCheckpointsAndDistance = view.findViewById(R.id.tvCheckpointsAndDistance);

        tvRerunNo.setText(String.format(Locale.ENGLISH, "Rerun #%d", i + 1));
        tvCheckpointsAndDistance.setText(String.format(Locale.ENGLISH, "%d checkpoints, %f meters", rerun.getCheckpoints().size(), rerun.getDistance()));

        return view;
    }
}
