package com.feko.runningtracker.helpers;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkHelper {
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
