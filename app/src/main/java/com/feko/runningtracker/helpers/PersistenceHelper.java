package com.feko.runningtracker.helpers;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.feko.runningtracker.persistence.RerunDatabase;

public class PersistenceHelper {

    private static RerunDatabase database;

    public static RerunDatabase getDatabaseInstance(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context, RerunDatabase.class, "rerun")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }
}
