package com.feko.runningtracker.helpers;

import android.content.Context;
import android.preference.PreferenceManager;

public class SharedPreferencesHelper {
    public static String getString(String stringName, int mode, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(stringName, null);
    }

    public static void putString(String stringName, String stringValue, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(stringName, stringValue).apply();
    }
}
