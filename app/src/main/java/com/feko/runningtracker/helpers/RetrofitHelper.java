package com.feko.runningtracker.helpers;

import android.content.Context;

import com.feko.runningtracker.R;

import retrofit2.Converter;
import retrofit2.Retrofit;

public class RetrofitHelper {
    public static <T> T getRetrofitInstance(Converter.Factory converterFactory, Class<T> retrofitInterface, Context context) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getString(R.string.api_url))
                .addConverterFactory(converterFactory)
                .build();

        return retrofit.create(retrofitInterface);
    }

}
