package com.feko.runningtracker.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;

import com.feko.runningtracker.R;
import com.feko.runningtracker.activities.RunningActivity;
import com.feko.runningtracker.helpers.PersistenceHelper;
import com.feko.runningtracker.persistence.RerunDatabase;
import com.feko.runningtracker.persistence.models.Checkpoint;
import com.feko.runningtracker.persistence.models.Rerun;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunningService extends Service implements LocationListener {
    private final int NOTIFICATION_NUMBER = 1;
    private List<Checkpoint> checkpoints;
    private int timer;
    private boolean shouldRun;
    private Notification.Builder builder;

    public RunningService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startRun();
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public void onDestroy() {
        shouldRun = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (checkpoints.size() == 0) {
                    return;
                }
                Rerun rerun = new Rerun();
                RerunDatabase database = PersistenceHelper.getDatabaseInstance(RunningService.this);
                database.rerunDao().save(rerun);
                for (Checkpoint checkpoint : checkpoints) {
                    checkpoint.setRerunId(rerun.getId());
                    database.checkpointDao().save(checkpoint);
                }
                stopForeground(true);
            }
        }).start();
    }

    private void startRun() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            // do nothing
        } else {
            checkpoints = new ArrayList<>();
            Intent runningIntent = new Intent(this, RunningActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addNextIntentWithParentStack(runningIntent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            builder = new Notification.Builder(getApplicationContext());
            builder.setContentTitle("Running...");
            startCounter();
            builder.setContentText(getTime(timer));
            builder.setContentIntent(pendingIntent);
            builder.setOngoing(true);
            builder.setSmallIcon(android.R.color.transparent);
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.rerun_icon));
            Notification notification = builder.build();
            startForeground(NOTIFICATION_NUMBER, notification);
            if (isNetworkEnabled) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5, 0, this);
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5, 0, this);
            }
        }
    }

    private void startCounter() {
        timer = 0;
        shouldRun = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (shouldRun) {
                    try {
                        Thread.sleep(1000);
                        timer++;
                        builder.setContentText(getTime(timer));
                        startForeground(NOTIFICATION_NUMBER, builder.build());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private String getTime(int timer) {
        return String.format("%02d:%02d", timer / 60, timer % 60);
    }

    @Override
    public void onLocationChanged(Location location) {
        Checkpoint checkpoint = new Checkpoint();
        checkpoint.setLat(location.getLatitude());
        checkpoint.setLng(location.getLongitude());
        checkpoint.setAlt(location.getAltitude());
        checkpoint.setTimestamp(new Date());
        checkpoints.add(checkpoint);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
